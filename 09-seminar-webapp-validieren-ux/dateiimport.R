# Hier möchte ich alle Dateien für meine Shiny App laden.
# Eventuell aus einer API oder einer Excel-Datei oder CSV …

# API Abruf ---------------------------------------------------------------

data_ch_post <- read_csv2(
    file = "https://swisspost.opendatasoft.com/api/v2/catalog/datasets/vornamen_proplz/exports/csv",
    col_types = cols(
        stichdatum = col_date(format = ""),
        plz = col_double(),
        geschlecht = col_character(),
        vorname = col_character(),
        anzahl = col_double(),
        rang = col_double(),
        ortbez18 = col_character()
    )
)

data_ch_post

# Erste Versuche

data_ch_post %>%
    select(-stichdatum) %>%
    filter(ortbez18 == "Luzern") %>%
    arrange(desc(anzahl))

data_ch_post %>%
    select(geschlecht, vorname, anzahl) %>%
    mutate(vorname = na_if(vorname, "n/a")) %>%
    drop_na() %>%
    group_by(geschlecht, vorname) %>%
    summarise_at(vars(anzahl), sum) %>%
    filter(anzahl > 50000) %>%
    arrange(desc(anzahl))

# Daten für die Webapp aufbereiten ----------------------------------------

data_shiny <-
    data_ch_post %>%
    mutate(vorname = na_if(vorname, "n/a")) %>%
    select(-rang, ort = ortbez18)

data_shiny

# Dateien speichern -------------------------------------------------------

write_rds(data_shiny, "data/data-shiny.rds", compress = "none")
