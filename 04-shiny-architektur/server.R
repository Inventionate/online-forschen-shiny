

# Backendlogik ------------------------------------------------------------

# Define server logic required to draw a histogram
server <- function(input, output) {

    # Tabelle -----------------------------------------------------------------

    # Das NULL-Problem?
    # ?reactive (Expressions)

    df_post_shiny <- reactive({
        # if (is.null(input$cols)) return(NULL)
        # df_post %>%
        # mutate(stichdatum = as.character(stichdatum)) %>%
        #     slice(1:input$bins) %>%
        #     select(input$cols)

        # Alternativ:
        # req()
        df_post %>%
            mutate(stichdatum = as.character(stichdatum)) %>%
            slice(1:input$bins) %>%
            select(req(input$cols))
    })

    observe(print(df_post_shiny()))

    observeEvent(
        input$bins,
        warning("Jemand hat die Zeilenanzahl verändert!")
    )

    output$dfPost <- renderTable({
        df_post_shiny()
    })


    # Plot --------------------------------------------------------------------

    df_plot_shiny <- reactive({

    })

    output$plotNames <- renderPlot({
        p <-
            ggplot(data = df_plot) +
            geom_col(aes(vorname, anzahl))

        if (input$cowtheme) {
            p <- p + cowplot::theme_minimal_hgrid()
        }

        p
    })

}
