myModuleUI <- function(id) {
    tagList(
        fluidRow(
            column(
                width = 4,
                h1("MODUL"),
                selectInput(
                    inputId = NS(id,"cols"),
                    label = "Spalten:",
                    choices = c(
                        "Stichsatum" = "stichdatum",
                        "PLZ" = "plz",
                        "geschlecht",
                        "vorname",
                        "anzahl",
                        "ort"
                    ),
                    multiple = TRUE,
                    selected = c("plz", "vorname", "ort")
                ),
                hr(),
                h2("MODUL"),
                sliderInput(
                    inputId = NS(id,"bins"),
                    label = "Slice:",
                    min = 1,
                    max = 30,
                    value = 10
                )
            ),
            column(
                width = 8,
                tableOutput(NS(id, "dfPost"))
            )
        )
    )
}

myModuleServer <- function(id) {
    moduleServer(id, function(input, output, session) {

        df_post_shiny <- reactive({
        df_post %>%
            mutate(stichdatum = as.character(stichdatum)) %>%
            slice(1:input$bins) %>%
            select(req(input$cols))
    })

    output$dfPost <- renderTable({
        df_post_shiny()
    })
    })
}
