# ******************************************************************************
# Datenstrukturen in R
# ******************************************************************************

# Vektoren ---------------------------------------------------------------------

c(9, 2, 3)

vector_1 <- c(1, 3, 5)
vector_1

vector_2 <- c("Hello", "News", "Some text here")
vector_2

vector_3 <- c(7, "some text", 4, TRUE, "other text …")
vector_3

# @INFO: Strings mit Variablen dynamisieren ------------------------------------

output_message <- "Das ist eine wichtige Nachricht! Es haben sich 17 neue Nutzer/-innen angemeldet."
output_message

new_users <- 17
new_users <- 78

output_message <- stringr::str_glue("Das ist eine wichtige Nachricht! Es haben sich { new_users } angemeldet.")
output_message

# @INFO: Funktionen! -----------------------------------------------------------

out <- "no"

output_message <- function(out = "NIX", zwei) {

    paste("Hello", out, zwei)

}

output_message(
    out = 16161,
    zwei = "ja, ja"
)

output_message <- function(
                           # Funktionsparameter, ggf. mit Standardwert.
                           new_users = 0) {
    # Funktionskörper mir R Code.
    # Wichtig:
    # Die letzte Zeile ist immer der Rückgabewert. Es ist kein expliziter return()-Aufruf nötig.
    # Dieser wird in R nur dann benötigt, wen vorzeitige Rückgabewerte nötig werden.
    message(stringr::str_glue("Das ist eine wichtige Nachricht! Es haben sich { new_users } angemeldet."))
}

output_message(new_users = 171)

# Schreiben wir nun unsere Funktion so um, dass im Fall 0 neuer Nutzer/-innen eine andere Ausgabe erfolgt.
output_message2 <- function(new_users = 0) {
    message <- "Das ist eine wichtige Nachricht!"

    if (new_users == 0) {
        return(
            warning(message, "Es haben sich keine neuen Nutzer/-innen angemeldet!")
        )
    }

    message(stringr::str_glue("{ message } Es haben sich { new_users } angemeldet."))
}
output_message2()

# Anonyme Funktionen
sapply(vector_1, function(value) {
    value * 3
})

# Data frames ------------------------------------------------------------------

df_1 <- data.frame(
    id = c(10:16),
    "land in Europa" = c(rep("CH", 4), rep("DE", 3))
)

# Base R subsetting
df_1[1, 2]

# Spaltennamen
colnames(df_1)

# Zeilennamen
rownames(df_1)

df_1

# @INFO: Tibbles als "moderne" / data frames
df_1_tbl <- tibble::tibble(
    id = c(10:16),
    "land in Europa" = c(rep("CH", 4), rep("DE", 3))
)

# Base R subsetting
df_1_tbl[1, 2]

# Spaltennamen
colnames(df_1_tbl)

# Zeilennamen
rownames(df_1_tbl)

df_1_tbl

# Listen -----------------------------------------------------------------------

output_object <- list(
    calc_1 = df_1,
    calc_2 = df_1_tbl,
    value = vector_3,
    message = output_message2(190)
)

output_object$calc_1

# Ausblick R 4.1 ---------------------------------------------------------------

# Native Pipe-Syntax: |>

# R < 4.1
mean(vector_1)
# R > 4.0
# vector_1 |> mean()

# Anonyme Funktionen: \()

# R < 4.1
sapply(vector_1, function(value) value * 3)

# R > 4.0
# sapply(vector_1, \(value) value * 3)

