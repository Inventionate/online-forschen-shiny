# ******************************************************************************
# Datenimport
# ******************************************************************************

# Pakete laden -----------------------------------------------------------------

library(tidyverse)
library(jsonlite)

# Datenimport ------------------------------------------------------------------

# Klassisches Laden von gespeicherten Dateien
data_ess <- read_csv("../data/ess.csv")
data_ess

plot_new <- read_rds("../repository/sample-plot-mtcars.rds")
plot_new

# Laden von Web-Daten
data_ch_post <- read_csv2("https://swisspost.opendatasoft.com/api/v2/catalog/datasets/vornamen_proplz/exports/csv")
data_ch_post

# Nutzen von APIs
api_token <- fromJSON("01-r-refresher/security.json")

df_schools_raw <-
    fromJSON(
        paste0("https://www.lemas-forschung.de/api/schools?api_token=", api_token)
    )

df_schools_raw %>% View()

df_schools <-
    df_schools_raw$data %>%
    as_tibble()

df_schools
