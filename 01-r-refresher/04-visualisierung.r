# ******************************************************************************
# Datenimport
# ******************************************************************************

# Pakete laden -----------------------------------------------------------------

library(tidyverse)
library(ggrepel)
library(cowplot)

# Daten laden ------------------------------------------------------------------

data_ch_post <- read_rds("repository/data-ch-post.rds")

# Daten aufbnereiten -----------------------------------------------------------

df_plot <-
    data_ch_post %>%
    filter(vorname %in% c("Alain", "Walter", "Véronique", "Urs", "Ruth", "Roberto")) %>%
    group_by(vorname) %>%
    summarise_at(vars(anzahl), sum)
df_plot

# Daten visualisieren ----------------------------------------------------------

ggplot(data = df_plot) +
    geom_col(aes(vorname, anzahl))

# Ein komplexeres Beispiel -----------------------------------------------------

df_motive_allgemein <-
    tibble(
        semester = c("00/01", "03/04", "05/06", "07/08", "09/10", "11/12"),
        Fachinteresse = c(91, 91, 92, 90, 91, 91),
        Neigung = c(85, 87, 86, 86, 85, 85),
        "Wiss. Interesse" = c(46, 47, 48, 43, 42, 43),
        "Anderen helfen" = c(28, 29, 32, 32, 35, 33),
        "Berufl. Sicherheit" = c(61, 69, 71, 68, 70, 68),
        "Status des Berufs" = c(49, 55, 56, 53, 53, 55),
        "Fester Berufswunsch" = c(60, 62, 62, 57, 56, 53),
        "Kurze Studienzeit" = c(8, 7, 8, 8, 3, 2)
    ) %>%
    pivot_longer(cols = 2:9) %>%
    mutate(
        name = factor(
            name,
            levels = c(
                "Fachinteresse",
                "Neigung",
                "Berufl. Sicherheit",
                "Status des Berufs",
                "Fester Berufswunsch",
                "Wiss. Interesse",
                "Anderen helfen",
                "Kurze Studienzeit"
            )
        ),
        group = "Gesamtheit der Studierenden"
    )

df_motive_lehramt <-
    tibble(
        semester = c("00/01", "03/04", "05/06", "07/08", "09/10", "11/12"),
        Fachinteresse = c(87, 90, 90, 89, 89, 91),
        Neigung = c(92, 92, 92, 91, 90, 93),
        "Wiss. Interesse" = c(28, 30, 32, 24, 26, 24),
        "Anderen helfen" = c(27, 65, 62, 67, 76, 79),
        "Berufl. Sicherheit" = c(62, 76, 79, 75, 83, 82),
        "Status des Berufs" = c(29, 35, 43, 34, 42, 40),
        "Fester Berufswunsch" = c(81, 80, 81, 76, 75, 78),
        "Kurze Studienzeit" = c(5, 5, 5, 4, 2, 0)
    ) %>%
    pivot_longer(cols = 2:9) %>%
    mutate(
        name = factor(
            name,
            levels = c(
                "Fachinteresse",
                "Neigung",
                "Berufl. Sicherheit",
                "Status des Berufs",
                "Fester Berufswunsch",
                "Wiss. Interesse",
                "Anderen helfen",
                "Kurze Studienzeit"
            )
        ),
        group = "Lehramtstudierende"
    )

df_motive <-
    bind_rows(df_motive_allgemein, df_motive_lehramt) %>%
    mutate(group = factor(group, levels = c("Gesamtheit der Studierenden", "Lehramtstudierende")))

plot_motive <-
    ggplot(
        df_motive,
        aes(semester, value)
    ) +
    geom_line(aes(colour = name, group = name)) +
    geom_text_repel(
        data = df_motive %>% filter(semester == "11/12"),
        aes(label = name, colour = name),
        family = "Fira Sans",
        vjust = 0.5,
        hjust = 0,
        nudge_x = 0.25,
        segment.alpha = 0,
        size = 4.5,
        box.padding = 0.15
    ) +
    geom_point(
        aes(colour = name),
        size = 4,
        shape = 15
    ) +
    theme_minimal_hgrid(font_family = "Fira Sans Condensed") +
    scale_x_discrete(
        breaks = df_motive$semester,
        labels = df_motive$semester,
        expand = expansion(add = c(0.25, 4))
    ) +
    scale_y_continuous(
        limits = c(0, 100),
        labels = paste(c(0, 25, 50, 75, 100), "%")
    ) +
    scale_color_brewer(palette = "Dark2") +
    labs(
        y = "Angaben in Prozent"
    ) +
    theme(
        legend.position = "none",
        axis.line.x = element_blank(),
        axis.ticks.x = element_blank(),
        axis.title.y = element_blank(),
        axis.title.x = element_blank(),
        plot.caption = element_text(margin = margin(20, 0, 0, 0)),
        axis.text = element_text(size = 12, colour = "gray30"),
        strip.text = element_text(size = 15, face = "bold"),
        panel.spacing = unit(2, "lines")
    ) +
    facet_wrap(~group) +
    coord_fixed(1 / 15)

plot_motive

# Speichern --------------------------------------------------------------------

write_rds(plot_motive, "repository/plot_motive.rds")
