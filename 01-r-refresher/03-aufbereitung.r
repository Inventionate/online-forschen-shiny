# ******************************************************************************
# Datenimport
# ******************************************************************************

# Pakete laden -----------------------------------------------------------------

library(tidyverse)

# Daten laden ------------------------------------------------------------------

data_ch_post <- read_csv2("https://swisspost.opendatasoft.com/api/v2/catalog/datasets/vornamen_proplz/exports/csv")
data_ch_post

# Daten aufbereiten ------------------------------------------------------------

data_ch_post %>%
    select(-stichdatum)

data_ch_post %>%
    arrange(desc(anzahl))

data_ch_post %>%
    select(-stichdatum) %>%
    filter(ortbez18 == "Luzern") %>%
    arrange(desc(anzahl))

data_ch_post %>%
    select(geschlecht, vorname, anzahl) %>%
    mutate(vorname = na_if(vorname, "n/a")) %>%
    drop_na() %>%
    group_by(geschlecht, vorname) %>%
    summarise_at(vars(anzahl), sum) %>%
    filter(anzahl > 50000) %>%
    arrange(desc(anzahl))

data_ch_post %>%
    select(vorname, anzahl, ortbez18) %>%
    filter(vorname %in% c("Maria", "Daniel", "Peter", "Thomas")) %>%
    arrange(desc(anzahl)) %>%
    group_by(vorname) %>%
    filter(anzahl == max(anzahl))
