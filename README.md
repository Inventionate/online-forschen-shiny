# Online forschen mit Shiny & Co.<br> Responsive Datenerhebung, -auswertung und -präsentation mit R

Die kontinuierlich voranschreitende Digitalisierung geht Hand in Hand mit technischen Entwicklungen, die für sozialwissenschaftliche Zugänge vielversprechende Chancen und Möglichkeiten eröffnen. Insbesondere moderne Webtechnologien, deren Kern dynamisch-interaktive Anwendungen darstellen, regen zu innovativen forschungsstrategischen Ansätze an. Man denke etwa an dynamisch generierte Erhebungssettings, die sich adaptiv an die Befragten anpassen, oder an elaborierte  Dashboards, die komplexe Zusammenhänge anschaulich und vor allem interaktiv zugänglich machen. Im Rahmen des Seminars werden diese neuen Möglichkeiten interaktiver Ansätze anhand einer praxisnahen Vorgehensweise erkundet.

Unter Verwendung der frei verfügbaren Statistikumgebung R und des darauf aufbauenden Shiny Frameworks werden die Potenziale interaktiver Webanwendungen für den gesamten Forschungsprozess von der Datenerhebung über die -auswertung bis hin zur Präsentation in den Blick genommen.

Anhand vieler Beispiele – speziell konstruierte wie «real life» – werden die Grundlagen und Herausforderungen der interaktiven Webanwendungsentwicklung mit Shiny veranschaulicht. Darauf aufbauend wird eine komplexere Applikation entwickelt, die die einzelnen Bestandteile integrativ verbindet und Raum für die individuelle Auseinandersetzung bietet. Auf diese Weise soll das Seminar in die Lage versetzen, eigene Webanwendungen mit R und Shiny zu konzipieren und zu realisieren. Die Entwicklung einer eben solchen stellt die aktive Seminarleistung dar.

## Literatur

- RStudio Shiny online Artikel: https://shiny.rstudio.com/articles 
- Sievert, Carson (2020): Interactive Web-Based Data Visualization with R. CRC Press
- Wickham, Hadley (2021): Mastering Shiny. O’Reilly. https://mastering-shiny.org
- Wickham, Hadley & Garrett Grolemund (2017): R for Data Science. O’Reilly. https://r4ds.had.co.nz
- Xie, Yihui, J. J. Allaire & Garrett Grolemund (2019): R Markdown. CRC Press. https://bookdown.org/yihui/rmarkdown/

## Ergänzende Materialien

- https://socviz.co
- https://www.edwardtufte.com
- https://github.com/jalvesaq/Nvim-R
- https://shinydevseries.com
- https://rinterface.com

## Advanced Shiny

- https://rinterface.github.io/shinyMobile/
- https://github.com/RinteRface/bs4Dash
- https://github.com/daattali/shinycssloaders#install
- https://deanattali.com/blog/advanced-shiny-tips/#loading-screen
- https://github.com/rstudio/shiny/issues/1321
- https://github.com/daattali/advanced-shiny/tree/master/url-inputs
- https://nteetor.github.io/yonder/

## Auth-Systeme

- Spezielles Paket: https://github.com/paulc91/shinyauthr/
- Skriptlösung: https://towardsdatascience.com/r-shiny-authentication-incl-demo-app-a599b86c54f7
- Auth0 Freemium-Service: https://github.com/curso-r/auth0
- Offene Shiny Server Open Source Alternative: https://www.shinyproxy.io

## R Markdown

- https://bookdown.org/yihui/rmarkdown-cookbook/
- https://bookdown.org/yihui/rmarkdown/
- https://pandoc.org/MANUAL.html

## Voraussetzungen

- Grundkenntnisse in R
- Basiswissen über Webtechnologien (HTML, CSS, JavaScript) ist vorteilhaft aber nicht zwingend erforderlich
